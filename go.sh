#!/usr/bin/env bash

docker run -ti \
  --rm \
  --hostname debian-dev \
  -p 80:80 \
  -v /home/jc/dev/site/pyblog:/home/dev/pyblog \
  -v /home/jc/dev/site/docker/nginx/sites-available/pyblog.conf:/etc/nginx/sites-available/pyblog.conf \
  -v /home/jc/dev/site/docker/nginx/sites-available/pyblog.conf:/etc/nginx/sites-enabled/pyblog.conf \
  -v /etc/adduser.conf:/etc/adduser.conf \
  -v /home/jc/dev/site/docker/scripts/bashrc-common:/root/.bashrc \
  -v /home/jc/dev/site/docker/scripts/bashrc-root:/root/bashrc-extended \
  -v /home/jc/dev/site/docker/scripts/bashrc-common:/home/jc/.bashrc \
  -v /home/jc/dev/site/docker/scripts/bashrc-jc:/home/jc/bashrc-extended \
  -v /home/jc/dev/site/docker/scripts/pyblog.sh:/pyblog.sh \
  debian-dev:stretch
