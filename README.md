# docker debian dev environment

I use this container config to dev django app with python3.6 and ruby for compass/sass.
It's a debian/stretch/stable based. Apt sourcelist is customized to enable backport and testing to install python 3.6+3.7

# HOW TO USE DEVELOPMENT ENVIRONMENT WITH A DOCKER CONTAINER


[1] DIRECTORY STRUCTURE
-----------------------
	site
	├── docker
	│   ├── apt
	│   │   ├── backports
	│   │   └── testing
	│   ├── Dockerfile
	│   ├── go.sh
	│   ├── nginx
	│   │   └── conf.d
	│   └── scripts
	│       └── pyblog.sh
	└── pyblog (project)


[2] LOADING DOCKER CONTAINER
----------------------------
	- In the project directory example 'site' move to docker directory:
		jc@jc-debian:~$ cd dev/site/docker/

	- Run the 'go.sh' script: It will run Docker container with many parameters.
		jc@jc-debian:~/dev/site/docker$ ./go.sh
		root@debian-dev:/#

	- Inside the container, run the 'pyblog.sh' script: Go to pyblog project & virtualenv.
		root@debian-dev:/# source pyblog.sh 
		(venv36dk) root@debian-dev:/home/dev/pyblog# 


[3] DOCKERFILE SUMMARY
----------------------
	- Based on Debian Stretch container
	- Add backport and testing repository
	- Install a few tools like vim, screenfetch, mc, ranger
	- Python 3.6 & 3.7
	- Install dev tools like git, tig
	- Ruby, Sass and Compass
	- nginx


[4] go.sh SUMMARY
-----------------
	The ./go.sh script run the debian container with parameters:
	--rm to remove de container on exit.
	--hostname debian-dev
	-ti for interactive terminal
	-p 80:80
	   to share web port with host
	-v /home/jc/dev/site/docker/nginx/sites-available/pyblog.conf:/etc/nginx/sites-available/pyblog.conf \
	-v /home/jc/dev/site/docker/nginx/sites-available/pyblog.conf:/etc/nginx/sites-enabled/pyblog.conf \
	   nginx configuration
	-v /home/jc/dev/site/docker/scripts/bashrc-common:/root/.bashrc \
	-v /home/jc/dev/site/docker/scripts/bashrc-root:/root/bashrc-extended \
	   mount brashrc to user 'root'
	-v /home/jc/dev/site/docker/scripts/bashrc-common:/home/jc/.bashrc \
	-v /home/jc/dev/site/docker/scripts/bashrc-jc:/home/jc/bashrc-extended \
	   mount brashrc to user 'jc'
	-v /home/jc/dev/site/pyblog:/home/dev/pyblog 
	   to mount project directory
	debian:stretch 
	  Debian based image


[5] pyblog.sh SUMMARY
---------------------
	Inside the container, we can run a quick setup:
	- The user root bashrc-extended start nginx et create second user for dev  
	- At the user 'rrot' prompt type: login jc and pwd
	  It will go to project directory and load the project Python virtualenv
	Notes:
	The reason i use 2 separate users:
	- If not: The file generated with framework will be with 'root' rules.
	- The rules disturbe git when 'git checkout master'.

