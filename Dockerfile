FROM debian:stretch
MAINTAINER open1024
RUN apt-get update; apt-get install -y figlet

RUN figlet -f slant "apt config"
RUN echo "\n# stretch-backports \ndeb http://deb.debian.org/debian/ stretch-backports main contrib\n\n# stretch testing \ndeb http://deb.debian.org/debian/ testing main contrib\n\n" >> /etc/apt/sources.list
COPY apt/testing /etc/apt/preferences.d/

RUN figlet -f slant "apt update"; apt-get update

RUN figlet -f slant "tools"; \
    apt-get install -y vim screenfetch mc net-tools ranger wget p7zip

RUN figlet -f slant "python 3.6 + 3.7"; \
    apt-get install -y -t testing \
    python3.6 \
    python3.6-dev \
    python3.6-venv \
    python3.7 \
    python3.7-dev \
    python3.7-venv \
    python3-pip \
    python3-distutils \
    python3-distutils-extra

RUN figlet -f slant "libs compil"; \
    cd /root; \
    wget -q https://sourceforge.net/projects/snap7/files/1.4.2/snap7-full-1.4.2.7z;\
    p7zip -d snap7-full-1.4.2.7z; \
    cd /root/snap7-full-1.4.2/build/unix; \
    make -f x86_64_linux.mk all; \
    cp /root/snap7-full-1.4.2/build/bin/x86_64-linux/libsnap7.so /usr/lib/libsnap7.so; \
    cp /root/snap7-full-1.4.2/build/bin/x86_64-linux/libsnap7.so /usr/local/lib/libsnap7.so;

RUN figlet -f slant "git + tig"; \
    apt-get install -y git tig

RUN figlet -f slant "ruby"; \
    apt-get install -y ruby-full; \
    gem update --system;

RUN figlet -f slant "sass"; \
    gem install sass

RUN figlet -f slant "compass"; \
    gem install compass;

RUN figlet -f slant "nginx"; \
    apt-get install -y -t testing nginx; \
    service nginx start;
